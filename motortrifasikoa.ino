/*              KONTROL  MONOEGONKORRA
 
Hurrengo programak bi puntsagailuren irakurketa egiten du eta bi irteera (erreele) kontrolatzen ditu era monoegonkorrean. 
Honez gain, bi irteeta horien balioa beste hiru pinetara lotzen da, hauetan LED bana sartuz. 
   Ondoren Serie Monitorean idazten ditu balioak.
   Modified Joseba eta Ainhoa 2019
   CC. Creatives Commons.
*/

// Aldagaien deklarazioa      

int botoiaD= 5;
int relay_1= 8;
int balioaD = 1;

int botoiaB= 3;
int relay_2= 7;
int balioaB = 1;

int led= 13;



// Sarrera-Irteeren Inizializazioa

void setup() 
  {
 Serial.begin (9600);
 pinMode (botoiaD, INPUT);
 pinMode (relay_1, OUTPUT);
 digitalWrite(relay_1,HIGH); //Errelea 1 itzali

 pinMode (botoiaB, INPUT);
 pinMode (relay_2, OUTPUT);
 digitalWrite(relay_2,HIGH); //Errelea 2 itzali

 pinMode (led, OUTPUT);
 digitalWrite(led,HIGH); //LED PIZTU

 
  }

// Programa nagusia

void loop() {
 balioaD = digitalRead(botoiaD); //Pultsadorea irakurri
 balioaB = digitalRead(botoiaB); //Pultsadorea irakurri
 Serial.print ("D Pultsagailuaren balioa=  ");
 Serial.println (balioaD);
 Serial.print ("B Pultsagailuaren balioa=  ");
 Serial.println (balioaB);
 Serial.println ("");
  
 if (balioaD==0 && balioaB==0){
   digitalWrite(relay_1,HIGH); //Errelea kontrolatu
   digitalWrite(relay_2,HIGH); //Errelea kontrolatu
   digitalWrite(led,HIGH);
   }
 else { 
      if (balioaD==0){ 
        digitalWrite(relay_1,LOW);
        digitalWrite(led,LOW);
        } //Errelea kontrolatu}
      
      if (balioaB==0){ 
        digitalWrite(relay_2,LOW);
        digitalWrite(led,LOW);
        } //Errelea kontrolatu}
      
      if (balioaD==1 && balioaB==1){
            digitalWrite(relay_1,HIGH); //Errelea kontrolatu
            digitalWrite(relay_2,HIGH); //Errelea kontrolatu
            digitalWrite(led,HIGH);
            }
      }

 delay (0); 
}
